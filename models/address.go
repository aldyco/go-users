package models

import (
	"fmt"
	u "go-users/utils"

	"github.com/jinzhu/gorm"
)

type Address struct {
	gorm.Model
	Name   string `json:"name"`
	Street string `json:"street"`
	UserId uint   `json:"user_id"` //The user that this contact belongs to
}

/*
 This struct function validate the required parameters sent through the http request body
returns message and true if the requirement is met
*/
func (address *Address) Validate() (map[string]interface{}, bool) {

	if address.Name == "" {
		return u.Message(false, "Address name should be on the payload"), false
	}

	if address.Street == "" {
		return u.Message(false, "Address street should be on the payload"), false
	}

	if address.UserId <= 0 {
		return u.Message(false, "User is not recognized"), false
	}

	//All the required parameters are present
	return u.Message(true, "success"), true
}

func (address *Address) Exists() (map[string]interface{}, bool) {

	err := GetDB().Find(address).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return u.Message(false, "Address not found"), false
	}

	//All the required parameters are present
	return u.Message(true, "found"), true
}

func (address *Address) Create() map[string]interface{} {

	if resp, ok := address.Validate(); !ok {
		return resp
	}

	GetDB().Create(address)

	resp := u.Message(true, "success")
	resp["address"] = address
	return resp
}

func (address *Address) Update() map[string]interface{} {

	if resp, ok := address.Validate(); !ok {
		return resp
	}

	if resp, ok := address.Exists(); !ok {
		return resp
	}

	GetDB().Save(address)

	resp := u.Message(true, "success")
	resp["address"] = address
	return resp
}

func (address *Address) Delete() map[string]interface{} {

	if address.ID < 1 {
		return u.Message(false, "failed")
	}

	GetDB().Delete(address)

	resp := u.Message(true, "success")
	return resp
}

func GetAddress(id uint) *Address {

	address := &Address{}
	err := GetDB().Table("addresses").Where("id = ?", id).First(address).Error
	if err != nil {
		return nil
	}
	return address
}

func GetAddresses(user uint) []*Address {

	address := make([]*Address, 0)
	err := GetDB().Table("addresses").Where("user_id = ?", user).Find(&address).Error
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return address
}
