package controllers

import (
	"encoding/json"
	"go-users/models"
	u "go-users/utils"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var CreateAddress = func(w http.ResponseWriter, r *http.Request) {

	user := r.Context().Value("user").(uint) //Grab the id of the user that send the request
	address := &models.Address{}

	err := json.NewDecoder(r.Body).Decode(address)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	address.UserId = user
	resp := address.Create()
	u.Respond(w, resp)
}

var GetAddressFor = func(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	id, err := strconv.Atoi(params["userId"])
	if err != nil {
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	data := models.GetAddresses(uint(id))
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetMyAddress = func(w http.ResponseWriter, r *http.Request) {

	user := r.Context().Value("user").(uint) //Grab the id of the user that send the request
	// id, err := strconv.Atoi(params["id"])
	// if err != nil {
	// The passed path parameter is not an integer
	// u.Respond(w, u.Message(false, "There was an error in your request"))
	// return
	// }

	data := models.GetAddresses(user)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var UpdateAddress = func(w http.ResponseWriter, r *http.Request) {

	user := r.Context().Value("user").(uint) //Grab the id of the user that send the request

	address := &models.Address{}

	err := json.NewDecoder(r.Body).Decode(address)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	params := mux.Vars(r)
	id, err := strconv.Atoi(params["addressId"])
	if err != nil {
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	address.UserId = user
	address.ID = uint(id)
	resp := address.Update()
	u.Respond(w, resp)
}

var DeleteAddress = func(w http.ResponseWriter, r *http.Request) {

	user := r.Context().Value("user").(uint) //Grab the id of the user that send the request

	address := &models.Address{}

	params := mux.Vars(r)
	id, err := strconv.Atoi(params["addressId"])
	if err != nil {
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	address.UserId = user
	address.ID = uint(id)
	resp := address.Delete()
	u.Respond(w, resp)
}

var DeleteAddressFor = func(w http.ResponseWriter, r *http.Request) {

	address := &models.Address{}

	params := mux.Vars(r)
	id, err := strconv.Atoi(params["addressId"])
	if err != nil {
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	user, err := strconv.Atoi(params["userId"])
	if err != nil {
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	address.UserId = uint(user)
	address.ID = uint(id)
	resp := address.Delete()
	u.Respond(w, resp)
}
