# go-restapi

This project is created as coding test and golang learning for the author.
JWT is used as token to authenticate API request.

# Setting up

Please make sure go and postgresql is installed.
Create .env file with configuration containing this example

```
	db_name = postgres
	db_pass = postgres123
	db_user = postgres
	db_type = postgres
	db_host = localhost
	db_port = 5432
	token_password = thisIsTheJwtSecretPassword
```

