package main

import (
	"fmt"
	"go-users/app"
	"go-users/controllers"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func main() {

	router := mux.NewRouter()
	router.Use(app.JwtAuthentication) //attach JWT auth middleware

	// List of routes
	router.HandleFunc("/api/user/new", controllers.CreateAccount).Methods("POST")
	router.HandleFunc("/api/user/login", controllers.Authenticate).Methods("POST")
	router.HandleFunc("/api/address", controllers.CreateAddress).Methods("POST")
	router.HandleFunc("/api/address", controllers.GetMyAddress).Methods("GET")
	router.HandleFunc("/api/address/{addressId}", controllers.UpdateAddress).Methods("PATCH")
	router.HandleFunc("/api/address/{addressId}", controllers.DeleteAddress).Methods("DELETE")
	router.HandleFunc("/api/user-address/{userId}", controllers.GetAddressFor).Methods("GET")
	router.HandleFunc("/api/user-address/{userId}/{addressId}", controllers.DeleteAddressFor).Methods("DELETE")

	port := os.Getenv("PORT") //Get port from .env file, we did not specify any port so this should return an empty string when tested locally
	if port == "" {
		port = "8000" //localhost
	}

	fmt.Println(port)

	err := http.ListenAndServe(":"+port, router) //Launch the app, visit localhost:8000/api
	if err != nil {
		fmt.Print(err)
	}
}
